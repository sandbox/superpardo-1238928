<?php
/**
  *@file
  *tsnking api admin inc
*/

define('ID_MAX_LENGTH'  , 10);
define('NAME_MAX_LENGTH', 225);

function ranking_admin() {
  $header = array(
    array(), 
    array(  'data' => t('nid'), 'field' => 'nid'), 
    array(  'data' => t('name'), 'field' => 'name'), 
    array(  'data' => t('value'), 'field' => 'value'), 
    array(  'data' => t('type'), 'field' => 'type'), 
    array('data' => t('Operations'), 'colspan' => 2)
  );
  $query  = db_query("SELECT * FROM {ranking_games}");
  $query .= tablesort_sql( $header );

  $query_count = sb_query("SELECT count(*) FROM {ranking_games}");
  $result      = pager_query($query, 50, 0, $query_count);

  $destination = drupal_get_destination();

  while ($game = db_fetch_object($result)) {
    $form['nid'][$game->rid]   = array('#value' => $game->nid);
    $form['name'][$game->rid]  = array('#value' => $game->name);
    $form['value'][$game->rid] = array('#value' => $game->value);
    $form['type'][$game->rid]  = array('#value' => $game->type);
    $form['operations'][$game->rid]['edit'] = array('#value' => l(t('edit'), "admin/settings/ranking/$game->rid/edit", array('query' => $destination)));
    $form['operations'][$game->rid]['delete'] = array('#value' => l(t('delete'), "admin/settings/ranking/$game->rid/delete", array('query' => $destination)));
  }

  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;
}

/**
 * Form builder; Ranking edit game.
 *
 * @ingroup forms
 * @see ranking_edit_game_validate()
 * @see ranking_edit_game_submit()
 */

function ranking_edit_game() {
  $_rid = arg(3);
  $query = db_query("SELECT * FROM {ranking_games} WHERE rid = %d", $_rid);
  $result = db_query($query);
  while ($obj = db_fetch_object($result)) {
    $rid = $obj;
  }
  
  $form['rid'] = array('#type' => 'hidden', '#value' => $_rid);
  $form['id_node'] = array(
    '#type' => 'textfield',
    '#title' => t('id'),
    '#maxlength' => ID_MAX_LENGTH,
    '#size' => 15,
    '#required' => TRUE,
    '#default_value' => $rid->nid,
  );
  $form['nombre'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#value' => $rid->name,
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('value'),
    '#maxlength' => ID_MAX_LENGTH,
    '#size' => 15,
    '#required' => TRUE,
    '#default_value' => $rid->value,
  );
  $form['options'] = array(
    '#type' => 'radios',
    '#title' => t('Default options'),
    '#default_value' => $rid->type,
    '#options' => array(
      'time' => t('Time'),
      'points' => t('Points'),
    ),
    '#required' => TRUE,
    '#description' => t('Select the type of score of the game'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

function ranking_edit_game_validate($form, $form_state) {
        $id     = $form_state['values']['id_node'];
        $nombre = $form_state['values']['nombre'];
        $value  = $form_state['values']['value'];

        if (!is_numeric($id)) {
                form_set_error('id', "El id no es numerico");
        }

        if (!is_numeric($value)) {
                form_set_error('value', "El campo valor no es numerico");
        }

        if (preg_match('/[^a-zA-Z0-9_]/', $nombre)) {
                form_set_error('Nombre', "El nombre no debe contener caracteres especiales");
        }
}

function ranking_edit_game_submit($form, &$form_state) {
  $rid   = $form_state['values']['rid'];
  $nid   = $form_state['values']['id_node'];
  $name  = $form_state['values']['nombre'];
  $type  = $form_state['values']['options'];
  $value = $form_state['values']['value'];

  $query = db_query("UPDATE {ranking_games} SET nid = %d, name = '%s', value = '%s', type = '%s' WHERE rid = %d ", $nid, $name, $value, $type, $rid);
  $result = db_query($query);
  drupal_set_message(t('The table has been updated successful.'));
  $form_state['redirect'] = "admin/settings/ranking";
  return;
}

/**
 * Form builder; Ranking delete game.
 *
 * @ingroup forms
 * @see ranking_delete_game_validate()
 * @see ranking_delete_game_submit()
 */

function ranking_delete_game() {
  $_rid = arg(3);
  $query = db_query("SELECT * FROM {ranking_games} WHERE rid = %d", $_rid);
  $result = db_query($query);
  while ($obj = db_fetch_object($result)) {
    $rid = $obj;
  }

  $form['rid']  = array('#type' => 'hidden', '#value' => $rid->rid);
  $form['name'] = array('#type' => 'hidden', '#value' => $rid->name);

  $output = confirm_form(
    $form,
    t('Are you sure you want to delete this game?'), 
    'admin/settings/ranking',
    t('This action cannot be undone and all the scores would be deleted'),
    t('Delete'),
    t('Cancel')
  );

  return $output;
}

function ranking_delete_game_submit( $form, &$form_state ) {
  $rid  = $form_state['values']['rid'];
  $name = $form_state['values']['name'];
  $table = "ranking_$name";
  db_query('DELETE FROM {ranking_games} WHERE rid = %d', $rid);
  db_query("DROP TABLE $table");

  drupal_set_message(t('The game has been deleted.'));
  $form_state['redirect'] = 'admin/settings/ranking';
  return;
}

/**
 * Form builder; Ranking add game.  
 *
 * @ingroup forms
 * @see ranking_add_game_validate()
 * @see ranking_add_game_submit()
 */

function ranking_add_game() {
  $form['id_node'] = array(
    '#type' => 'textfield',
    '#title' => t('id'),
    '#maxlength' => ID_MAX_LENGTH,
    '#size' => 15,
    '#required' => TRUE,
  );
  $form['nombre'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => NAME_MAX_LENGTH,
    '#size' => 15,
    '#required' => TRUE,
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('value'),
    '#maxlength' => ID_MAX_LENGTH,
    '#size' => 15,
    '#required' => TRUE,
    '#description' => t('El valor de cada punto para los userpoints'),
  );
  $form['options'] = array(
    '#type' => 'radios', 
    '#title' => t('Default options'), 
    '#default_value' => 'time',
    '#options' => array(
      'time' => t('Time'), 
      'points' => t('Points'), 
    ),
    '#required' => TRUE,
    '#description' => t('Select the type of score of the game'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

function ranking_add_game_validate($form, $form_state) {
  $id     = $form_state['values']['id_node'];
  $value  = $form_state['values']['value'];
  $nombre = $form_state['values']['nombre'];

  if (!is_numeric($id)) {
    form_set_error('id', "El id no es numerico");
  }

  if (!is_numeric($value)) {
    form_set_error('value', "El valor no es numerico");
  }

  if (preg_match('/[^a-zA-Z0-9_]/', $nombre)) {
    form_set_error('Nombre', "El nombre no debe contener caracteres especiales");
  }
}

/**
 * Submit the game ranking add form.
 */

function ranking_add_game_submit($form, &$form_state) {
  $name  = $form_state['values']['nombre'];
  $type  = $form_state['values']['options'];
  $nid   = $form_state['values']['id_node'];
  $value = $form_state['values']['value'];

  $query = db_query("SELECT count(*) AS count from {ranking_games} WHERE name = '%s' OR nid = %d", $name, $nid);

  $result = db_query($query);
  while ($obj = db_fetch_object($result)) {
  $validate = $obj->count;
  }

  if ($validate > 0) {
    form_set_error('table', "Un juego con ese nombre o con ese nid ya ha sido registrado");
  }
  else{
    $table = "ranking_$name";
    $query = db_query("INSERT INTO {ranking_games} (rid, nid, name, value, type) VALUES(NULL, %d, '%s', %d, '%s')", NULL, $nid, $name, $value, $type);
    $result = db_query( $query );

    if ( $type == "points" ) {
      $query = db_query("CREATE TABLE  $table (
				`id` INT NOT NULL AUTO_INCREMENT,
				`id_game` INT NOT NULL ,
				`id_user` INT NOT NULL ,
				`score` DOUBLE NOT NULL ,
				`date` INT NOT NULL ,
				PRIMARY KEY (  `id` ) ,
				INDEX (  `id_user` ,  `score` ,  `date` )
			)");
   }
   else{
     $query = db_query("CREATE TABLE  $table (
				`id` INT NOT NULL AUTO_INCREMENT,
				`id_game` INT NOT NULL ,
				`id_user` INT NOT NULL ,
				`score` VARCHAR(120) NOT NULL ,
				`date` INT NOT NULL ,
				PRIMARY KEY (  `id` ) ,
				INDEX (  `id_user` ,  `score` ,  `date` )
			)");
   }

    $result = db_query( $query );
    drupal_set_message(t('The table has been created successful.'));
  }

}


/**
 * Theme user administration overview.
 *
 * @ingroup themeable
 */

function theme_ranking_admin($form) {
  $header = array(
    array(  'data' => t('nid'), 'field' => 'nid'),
    array(  'data' => t('name'), 'field' => 'name'),
    array(  'data' => t('value'), 'field' => 'value'),
    array(  'data' => t('type'), 'field' => 'type'),
    array('data' => t('Operations'), 'colspan' => 2)
  );

  if ( isset($form['nid']) ) {
     foreach (element_children($form['nid']) as $key) {
      $rows[] = array(
        drupal_render($form['nid'][$key]), 
        drupal_render($form['name'][$key]), 
        drupal_render($form['value'][$key]), 
        drupal_render($form['type'][$key]), 
        drupal_render($form['operations'][$key]['edit']),
        drupal_render($form['operations'][$key]['delete']),
      );
    } 
  } 
  else { 
   $rows[] = array(array('data' => t('No games available.'), 'colspan' => '7'));  
  }

  $output .= theme('table', $header, $rows);

  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']); 
  }

  $output .= drupal_render($form);
  return $output;
}


